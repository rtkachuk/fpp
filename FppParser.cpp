#include "FppCompiler.h"

QString parseImports(QString buff)
{
	if (buff.contains(QString("%1 ").arg(C_INCLUDE)))
	{
		buff.remove(QString("%1 ").arg(C_INCLUDE));
		buff.remove(";");
		buff = "#include <" + buff + ">";
	}
	return buff;
}

QString parseVariables(QString buff)
{
	if (buff.contains(QString("%1 ").arg(C_INT)) ||
			buff.contains(QString("%1 ").arg(C_FLOAT)) ||
			buff.contains(QString("%1 ").arg(C_STRING)))
	{
		buff.replace(QString("%1 ").arg(C_FLOAT), "float ");
		buff.replace(QString("%1 ").arg(C_STRING), "string ");
		buff.replace(QString("%1 ").arg(C_INT), "int ");
	}

	return buff;
}

QString parseLogic(QString buff)
{
	if (buff.contains(QString("%1 ").arg(C_IF)))
	{
		buff.replace(QString("%1 ").arg(C_IF), "if (");
		buff.replace(QString(" %1 ").arg(C_LOGIC_AND), " && ");
		buff.replace(QString(" %1 ").arg(C_LOGIC_IS), " == ");
		buff.replace(QString(" %1 ").arg(C_LOGIC_LESS), " < ");
		buff.replace(QString(" %1 ").arg(C_LOGIC_MORE), " > ");
		buff.replace(QString(" %1 ").arg(C_LOGIC_NOT), " != ");
		buff.replace(QString(" %1 ").arg(C_LOGIC_OR), " || ");
		buff += ")";
	}

	return buff;
}

QString parseIO(QString buff)
{
	if (buff.contains(C_IO_COUT))
	{
		buff.replace(C_IO_COUT, "cout ");
		buff.replace("->", "<<");
	}

	if (buff.contains(C_IO_CIN))
	{
		buff.replace(C_IO_CIN, "cin ");
		buff.replace("->",">>");
	}

	return buff;
}

QString parseBrackets(QString buff)
{
	if (buff.contains(C_OPEN_BRACKET))
	{
		buff.replace(C_OPEN_BRACKET, "{");
	}

	if (buff.contains(C_CLOSE_BRACKET))
	{
		buff.replace(C_CLOSE_BRACKET, "}");
	}

	if (buff.contains(C_ELSE))
		buff.replace(C_ELSE, "else");

	return buff;
}

bool FppCompiler::parseSyntax()
{
	for (int lineIndex = 0; lineIndex < m_foxProgram.size(); lineIndex++)
	{
		if (m_foxProgram[lineIndex].contains(C_INCLUDE) == false)
		{
			m_foxProgram.insert(lineIndex, "using namespace std;\nint main()\n{");
			break;
		}
	}

	QFile outputFile(m_inputFilePath + ".cpp");

	if (outputFile.open(QIODevice::WriteOnly) == false)
	{
		qDebug() << "Error opening output file: " << outputFile.errorString();
		return false;
	}

	QTextStream fileInterface(&outputFile);

	for (QString buffer : m_foxProgram)
	{
		buffer.remove("\n");
		buffer = parseImports(buffer);
		buffer = parseVariables(buffer);
		buffer = parseIO(buffer);
		buffer = parseLogic(buffer);
		buffer = parseBrackets(buffer);

		buffer += "\n";

		fileInterface << buffer;
	}

	fileInterface << "}\n";

	outputFile.close();

	qDebug() << "|-- OK";

	return true;
}
