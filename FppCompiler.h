#ifndef FPPCONFIGURE_H
#define FPPCONFIGURE_H

#include <QCoreApplication>
#include <QProcess>
#include <QDebug>
#include <QFile>

#define SOURCE_FILE_FORMAT ".fox"
#define C_INCLUDE "import"

/*  LANGUAGE TYPE DATA  */
#define C_INT "num"
#define C_FLOAT "fnum"
#define C_STRING "wor"

/*  OPERATORS CONDITIONS  */
#define C_IF "guess"
#define C_ELSE "but"
#define C_WHILE "cycle"

/*    BRACKETS    */
#define C_OPEN_BRACKET "begin"
#define C_CLOSE_BRACKET "end"

/*    LOGIC OPERATORS AND INCREMENT */  // Рома а почему нет декремента???
#define C_INC "inc"
#define C_LOGIC_IS "is"
#define C_LOGIC_MORE "ab"
#define C_LOGIC_LESS "le"
#define C_LOGIC_NOT "ne"
#define C_LOGIC_BECOME "bec"
#define C_LOGIC_AND "an"
#define C_LOGIC_OR "or"

#define C_IO_COUT "shw"
#define C_IO_CIN "ask"

typedef struct
{
    bool includeIO;
    bool callCout;
    bool callCin;

}CHECK_IO_T;

typedef struct
{
    uint32_t countBegin;
    uint32_t countEnd;
}COUNT_BRACKETS_T;

class FppCompiler
{
public:

	FppCompiler();

	void detectGpp();
	bool processArgument(QString argument);

	void setGpp(QString gppPath) { m_gppPath = gppPath; }
	QString getGpp() { return m_gppPath; }

	void setInputFile(QString filePath) { m_inputFilePath = filePath; }
	QString getInputFile() { return m_inputFilePath; }

	void setOutputFile(QString filePath) { m_outputFilePath = filePath; }
	QString getOutputFile() { return m_outputFilePath; }

	void setDebug(bool value) { m_isDebug = value; }
	bool isDebug() { return m_isDebug; }

	bool compile();

private:
	bool checkValues();
    bool checkSyntax();             // by shepard127
	bool parseSyntax();

	QString m_gppPath;
	QString m_inputFilePath;
	QString m_outputFilePath;

	QVector<QString> m_foxProgram;
	QString m_compileError;

	bool m_isDebug;
	
	CHECK_IO_T CHECK_IO;
    COUNT_BRACKETS_T COUNT_BRACKETS;
};

#endif // FPPCONFIGURE_H
