#include "FppCompiler.h"

FppCompiler::FppCompiler()
{
	m_isDebug = false;

}

void FppCompiler::detectGpp()
{
	QProcess proc;
	proc.start("which g++");

	if (proc.waitForFinished())
	{
		QString gppSearchOutput = proc.readAll();
		if (gppSearchOutput.contains("g++"))
			m_gppPath = gppSearchOutput.remove("\n");
	}
}

bool FppCompiler::processArgument(QString argument)
{
	if (argument.contains("-o=") || argument.contains("--output="))
	{
		argument.remove("-o=");
		argument.remove("--output=");
		setOutputFile(argument);

		return true;
	}

	if (argument.contains("-d") || argument.contains("--debug"))
	{
		argument.remove("-d");
		argument.remove("--debug");
		setDebug(true);

		return true;
	}

	if (argument.contains("-c=") || argument.contains("--compiler="))
	{
		argument.remove("-c=");
		argument.remove("--compiler=");
		setGpp(argument);

		return true;
	}

	return false;
}

bool FppCompiler::compile()
{
	qDebug() << "|- Prepare step started: ";
	if (checkValues() == false)
	{
		qDebug() << "|- Prepare step failed!";
		return false;
	}

	qDebug() << "|- Check syntax step started: ";
	if (checkSyntax() == false)
	{
		qDebug() << "|- Check step failed!";
		return false;
	}

	qDebug() << "|- Parse syntax step started: ";
	if (parseSyntax() == false)
	{
		qDebug() << "|-- Parse step failed!";
		return false;
	}

	qDebug() << "|- Running g++...";

	QString commandForGpp = QString("g++ %1 -o %2").arg(m_inputFilePath + ".cpp").arg(m_outputFilePath);

	int ok = system(&commandForGpp.toStdString()[0]);
	if (ok == 0)
		qDebug() << "|-- OK";
	else
		qDebug() << "|-- FAIL";

	qDebug() << "All seems to be ok now.";

	return true;
}

bool FppCompiler::checkValues()
{
	if (getGpp().isEmpty())
	{
		detectGpp();
		if (getGpp().isEmpty())
		{
			qDebug() << "|-- Error: g++ not found!";
			return false;
		}
	}

	if (getOutputFile().isEmpty())
	{
		setOutputFile("output");
	}

	qDebug() << "|-- OK";

	return true;
}

bool FppCompiler::checkSyntax()
{
   QFile checkFile("test.fox");

    if(!checkFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "Error open file" << checkFile.errorString();
        return false;
    }

    QVector<QString> code;

    QString lineFromFile = checkFile.readLine();
    while(!lineFromFile.isNull())
    {
        code.append(lineFromFile);
        lineFromFile = checkFile.readLine();
    }


	const int sizeCode = code.size();
    int i = 0;

    /*--------------- IOSTREAM  -----------------*/
	while(i < sizeCode)
    {
        QString tempStr = code[i];
        int resultSearch = tempStr.indexOf("import iostream;");
        if(resultSearch >= 0)
            CHECK_IO.includeIO = true;
        i++;
    }

    i = 0;  // detection calling IO operators
    while(i < sizeCode)
    {
        QString tempStr = code[i];
        int resultSearch = tempStr.indexOf("shw");
        if(resultSearch >= 0)
            CHECK_IO.callCout = true;

        resultSearch = tempStr.indexOf("ask");
        if(resultSearch >= 0)
            CHECK_IO.callCin = true;
        i++;
    }

    if((CHECK_IO.callCin || CHECK_IO.callCout) && !CHECK_IO.includeIO)
    {
        qDebug() << "Error IO operation!";
        return -1;
    }
    /*------------  BRACKETS  --------------*/
	/*i = 0;
    while(i < sizeCode)
    {
        QString tempStr = code[i];
        int resultSearch = tempStr.indexOf("begin");
        if(resultSearch >= 0)
            COUNT_BRACKETS.countBegin++;

        resultSearch = tempStr.indexOf("end");
        if(resultSearch >= 0)
            COUNT_BRACKETS.countEnd++;

        i++;
    }

    if(COUNT_BRACKETS.countBegin != COUNT_BRACKETS.countEnd)
    {
        qDebug() << "Error brackets count";
        return -1;
	}*/


	
	// Димас, я тут добавил внизу пару строк, чтобы я мог уже обрабатывать ввод,
	// сорян плз
    // всё норм:)

	m_foxProgram = code;
	checkFile.close();
	qDebug() << "|-- OK";

	return true;
}
