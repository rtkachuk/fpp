#include <QCoreApplication>
#include <QProcess>
#include <QDebug>

#include "FppCompiler.h"

//=============================
//
// ERROR CODES:
//
// 0 - Ok
// 1 - Wrong argument amount (less than 1)
// 2 - g++ was not found
// 3 - Wrong source file format
// 4 - Argument processing error
//
//=============================


int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	if (argc < 2)
	{
		qDebug() << "Use: ./fpp {source name} {options}\n";
		qDebug() << "OPTIONS:\n";
		qDebug() << "\t -o= --output={FILE} : Output file";
		qDebug() << "\t -c= --compiler={PATH} : absolute path to g++";
		qDebug() << "\t -d --debug : Show all debug messages";
		return 1;
	}

	FppCompiler fpp;

	QString filePath = argv[1];
	if (filePath.contains(SOURCE_FILE_FORMAT) == false)
	{
		qDebug() << "Wrong source file format! Use *.fox format for source files!";
		return 3;
	}

	fpp.setInputFile(filePath);

	for (int argumentNumber = 2; argumentNumber < argc; argumentNumber++)
	{
		if (fpp.processArgument(argv[argumentNumber]) == false)
		{
			qDebug() << "Error in processing argument: " << argv[argumentNumber];
			return 4;
		}
	}

    fpp.compile();

	return 0;
}
