Fpp compiler  
==========================  
  
This compiler is interpritator-based utility, that  
can make your programming experience earning much easier  
and pleasureful.  
  
HOW TO  
==========================  
  
To compile test file with fpp compiler, you should  
build compiler with Qt, and then in command-line  
environtment run:  
  
fpp test.fox  
  
Fpp will try to find g++ compiler. You can specify it  
by yourself with passing command-line argument  
-c=pathToCompiler, or --compiler=pathToCompiler  
  
This compiler was tested only on Linux-based distros. But  
also can be ported to windows.
